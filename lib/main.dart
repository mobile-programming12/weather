import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            backgroundColor: Colors.blueGrey[50],
            appBar: AppBar(
              title: Text("Weather App"),
              backgroundColor: Colors.blue[300],
              leading: Icon(Icons.arrow_back),
              actions: <Widget>[
                TextButton(
                    onPressed: () {}, child:Text("ตำแหน่ง", style: TextStyle(color: Colors.white),) ),

                IconButton(onPressed: (){}, icon: Icon(Icons.add), ),


              ],
            ),
            body: ListView(
                // scrollDirection: Axis.horizontal,
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      Container(
                        width: double.infinity,
                        //Height constraint at Container widget level
                        height: 250,
                        child: Image.network(
                          "https://www.smileconsumer.com/wp-content/uploads/2012/09/1-IMG_6369-0011.jpg",
                          fit: BoxFit.cover,
                        ),

                      ),
                      Positioned(
                        top: 120,
                        left: 30,
                        child: Container(
                          //margin: EdgeInsets.only(top: 8, bottom: 8, left: 8),
                          //alignment: Alignment.,
                          child: Text(
                            'อำเภอเมืองชลบุรี',
                            style: TextStyle(color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 22.0),
                          ),
                        ),
                      ),
                      Positioned(
                        top: 150,
                        left: 20,
                        child: Container(
                          //margin: EdgeInsets.only(top: 8, bottom: 8, left: 8),
                          //alignment: Alignment.,
                          child: Text(
                            'อ. 27 ธันวาคม 14:58',
                            style: TextStyle(color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 22.0),
                          ),
                        ),
                      ),
                      Positioned(
                        left: 90,
                        top: 190,
                        child:
                        Container(
                            child: Text(
                              '29°',
                              style: TextStyle(color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 40),
                            )
                        ),
                      ),
                    ],
                  ),
                  // Container(
                  //   width: double.infinity,
                  //            //Height constraint at Container widget level
                  //   height: 100, child: Image.network("https://i.pinimg.com/564x/42/c4/7a/42c47ace15cc6881ed1c16a760790bfe.jpg",
                  //            ),
                  //          ),
                  Container(
                    margin: EdgeInsets.only(top: 8, bottom: 8,left: 50,right: 50),
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      color: Colors.grey.shade300,
                      border: Border.all(
                        color: Colors.blue.shade300,
                        width: 3,
                      ),
                      borderRadius: BorderRadius.all(
                        Radius.circular(50)
                      ),
                      boxShadow: [BoxShadow(blurRadius: 2,offset: Offset(1,5))]
                    ),
                    child: Theme(
                      data: ThemeData(
                          iconTheme: IconThemeData(
                            color: Colors.blue[300],
                          )
                      ),
                      child: rainUvItem(),
                    ),
                  ),

                  Container(
                      margin: EdgeInsets.only(left: 8),
                      alignment: Alignment.topLeft,
                      child: Text(
                        'ทุกชั่วโมง',
                        style: TextStyle(color: Colors.blue,
                            fontWeight: FontWeight.bold,
                            fontSize: 22.4),
                      )
                  ),

                  Container(
                    margin: EdgeInsets.only(top: 8, bottom: 8,left: 6, right: 6),
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        color: Colors.grey.shade300,
                        border: Border.all(
                          color: Colors.blue.shade300,
                          width: 3,
                        ),
                        borderRadius: BorderRadius.all(
                            Radius.circular(30)
                        ),
                        boxShadow: [BoxShadow(blurRadius: 2,offset: Offset(1,5))]
                    ),
                    child: Theme(
                      data: ThemeData(
                          iconTheme: IconThemeData(
                            color: Colors.blue[300],
                          )
                      ),
                      child: weatherItem(),
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 8),
                      alignment: Alignment.topLeft,
                      child: Text(
                        'รายวัน',
                        style: TextStyle(color: Colors.blue,
                            fontWeight: FontWeight.bold,
                            fontSize: 22.4),
                      )
                  ),
                  Container(

                    margin: EdgeInsets.only(top: 8, bottom: 8,left: 6, right: 6),
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        color: Colors.grey.shade300,
                        border: Border.all(
                          color: Colors.blue.shade300,
                          width: 3,
                        ),
                        borderRadius: BorderRadius.all(
                            Radius.circular(30)
                        ),
                        boxShadow: [BoxShadow(blurRadius: 2,offset: Offset(1,5))]
                    ),
                    child: Theme(
                      data: ThemeData(
                          iconTheme: IconThemeData(
                            color: Colors.blue[300],
                          )
                      ),
                      child: dayItem(),
                    ),
                  ),
                ]
            )
        )
    );
  }
}
class MyStatelessWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      // alignment: Alignment.topCenter,
      child: Container(
        height: 50,
        // padding: const EdgeInsets.all(10),
        child: VerticalDivider(
          color: Colors.blueGrey[300],
          thickness: 2,

        ),
      ),
    );
  }
}

Widget build14HrButton() {
  return Column(
    children: <Widget>[
      Text("14:00"),
      IconButton(
        icon: Icon(
          Icons.light_mode,
          color: Colors.orange,
        ),
        onPressed: () {},
      ),
      Row(
        children:<Widget> [
          Icon(Icons.cloudy_snowing, color: Colors.grey),
          Text(" 0%")
        ],
      ),
      Text("30°"),
    ],
  );
}
Widget build17HrButton() {
  return Column(
    children: <Widget>[
      Text("17:00"),
      IconButton(
        icon: Icon(
          Icons.light_mode,
          color: Colors.orange,
        ),
        onPressed: () {},
      ),
      Row(
        children:<Widget> [
          Icon(Icons.cloudy_snowing, color: Colors.grey),
          Text(" 0%")
        ],
      ),
      Text("28°"),
    ],
  );
}
Widget build20HrButton() {
  return Column(
    children: <Widget>[
      Text("20:00"),
      IconButton(
        icon: Icon(
          Icons.nights_stay,
          color: Colors.indigo[400],
        ),
        onPressed: () {},
      ),
      Row(
        children:<Widget> [
          Icon(Icons.cloudy_snowing, color: Colors.grey),
          Text(" 0%")
        ],
      ),
      Text("28°"),
    ],
  );
}
Widget build23HrButton() {
  return Column(
    children: <Widget>[
      Text("23:00"),
      IconButton(
        icon: Icon(
          Icons.cloud,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Row(
        children:<Widget> [
          Icon(Icons.cloudy_snowing, color: Colors.grey),
          Text(" 0%"),
        ],
      ),
      Text("28°"),
    ],
  );
}
Widget build02HrButton() {
  return Column(
    children: <Widget>[
      Text("02:00"),
      IconButton(
        icon: Icon(
          Icons.nightlight,
          color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
       Row(
         children:<Widget> [
           Icon(Icons.cloudy_snowing, color: Colors.grey),
           Text(" 0%")
         ],
       ),
       Text("28°"),
    ],
  );
}
Widget weatherItem(){
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      build14HrButton(),
      build17HrButton(),
      build20HrButton(),
      build23HrButton(),
      build02HrButton(),
    ],
  );
}
Widget buildRainButton() {
  return Row(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.beach_access_rounded,
          color: Colors.blue,
        ),
        onPressed: () {},
      ),
      Column(
        children:<Widget> [
          Text("มีฝน"),
          Text("1%"),
        ],
      ),
    ],
  );
}
Widget buildUVButton() {
  return
    Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      IconButton(
      icon: Icon(
        Icons.light_mode,
        color: Colors.orange,
      ),
      onPressed: () {},
    ),
      Column(
        children:<Widget> [
          Text("ดัชนี UV"),
          Text("สูงมาก"),
        ],
      ),

    ],
  );
}
Widget rainUvItem(){
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildRainButton(),
      MyStatelessWidget(),
      buildUVButton()
      // IntrinsicHeight(
      // child: Row(
      //     children: [
      // buildRainButton()
      //   ],
      // ),
      // ),
      // VerticalDivider(
      //   color: Colors.black,  //color of divider
      //   width: 10, //width space of divider
      //   thickness: 3, //thickness of divier line
      //   indent: 10, //Spacing at the top of divider.
      //   endIndent: 10, //Spacing at the bottom of divider.
      // ),
      // Expanded(child: buildUVButton()),
    ],
  );
}
Widget dayItem(){
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildTodayHrButton(),
      buildWenButton(),
      buildThuButton(),
      buildFriButton(),
      buildSatButton(),
    ],
  );
}
Widget buildTodayHrButton() {
  return Column(
    children: <Widget>[
      Text("วันนี้"),
      IconButton(
        icon: Icon(
          Icons.light_mode,
          color: Colors.orange,
        ),
        onPressed: () {},
      ),
      Row(
        children:<Widget> [
          Icon(Icons.cloudy_snowing, color: Colors.grey),
          Text(" 1%")
        ],
      ),
      Text("31°"),
    ],
  );
}
Widget buildWenButton() {
  return Column(
    children: <Widget>[
      Text("พ."),
      IconButton(
        icon: Icon(
          Icons.light_mode,
          color: Colors.orange,
        ),
        onPressed: () {},
      ),
      Row(
        children:<Widget> [
          Icon(Icons.cloudy_snowing, color: Colors.grey),
          Text(" 5%")
        ],
      ),
      Text("31°"),
    ],
  );
}
Widget buildThuButton() {
  return Column(
    children: <Widget>[
      Text("พฤ."),
      IconButton(
        icon: Icon(
          Icons.light_mode,
          color: Colors.orange,
        ),
        onPressed: () {},
      ),
      Row(
        children:<Widget> [
          Icon(Icons.cloudy_snowing, color: Colors.grey),
          Text(" 3%")
        ],
      ),
      Text("31°"),
    ],
  );
}
Widget buildFriButton() {
  return Column(
    children: <Widget>[
      Text("ศ."),
      IconButton(
        icon: Icon(
          Icons.light_mode,
          color: Colors.orange,
        ),
        onPressed: () {},
      ),
      Row(
        children:<Widget> [
          Icon(Icons.cloudy_snowing, color: Colors.grey),
          Text(" 2%")
        ],
      ),
      Text("31°"),
    ],
  );
}
Widget buildSatButton() {
  return Column(
    children: <Widget>[
      Text("ส."),
      IconButton(
        icon: Icon(
          Icons.light_mode,
          color: Colors.orange,
        ),
        onPressed: () {},
      ),
      Row(
        children:<Widget> [
          Icon(Icons.cloudy_snowing, color: Colors.grey),
          Text(" 3%")
        ],
      ),
      Text("30°"),
    ],
  );
}



